package com.dashboard.analytics.Service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dashboard.analytics.Dao.Agent;
import com.dashboard.analytics.Dao.ErrorNodejs;
import com.dashboard.analytics.Dao.ErrorWebsphere;
import com.dashboard.analytics.Dao.Relationships;
import com.dashboard.analytics.Repositories.RepositoryAgent;
import com.dashboard.analytics.Repositories.RepositoryErrorNodejs;
import com.dashboard.analytics.Repositories.RepositoryErrorWebsphere;

@Service
public class ServiceSpring {

	@Autowired
	RepositoryErrorNodejs repositoryerrorsNodejs;

	@Autowired
	RepositoryErrorWebsphere repositoryerrorsWebsphere;

	@Autowired
	RepositoryAgent repositoryagent;

	private Map<String, Object> toD3FormatErrorsNodejs(
			Collection<ErrorNodejs> errorsNodejs) {
		List<Map<String, Object>> nodes = new ArrayList<>();

		Iterator<ErrorNodejs> result = errorsNodejs.iterator();
		while (result.hasNext()) {
			ErrorNodejs errorNodejs = result.next();
			nodes.add(map(errorNodejs.getId().toString(), errorNodejs));

		}
		return map("nodes", nodes);
	}

	public Map<String, Object> toD3FormatErrorsWebsphere(
			Collection<ErrorWebsphere> errorsWebsphere) {
		List<Map<String, Object>> nodes = new ArrayList<>();

		Iterator<ErrorWebsphere> result = errorsWebsphere.iterator();

		while (result.hasNext()) {
			ErrorWebsphere errorWebsphere = result.next();
			nodes.add(map(errorWebsphere.getId().toString(), errorWebsphere));

		}

		return map("nodes", nodes);

	}

	private Map<String, Object> map(String key1, Object value1) {
		Map<String, Object> result = new HashMap<String, Object>(1);
		result.put(key1, value1);

		return result;
	}

	@Transactional(readOnly = true)
	public Map<String, Object> graph(int limit) {
		Collection<ErrorNodejs> result = repositoryerrorsNodejs.graph(limit);
		return toD3FormatErrorsNodejs(result);
	}

	@Transactional(readOnly = true)
	public Map<String, Object> getCallsMachine() {
		Collection<Agent> result = repositoryagent.returnStructureRest();
		return toD3FormatCalls(result);
	}

	private Map<String, Object> toD3FormatCalls(Collection<Agent> result2) {
		List<Map<String, Object>> nodes = new ArrayList<>();
		List<Map<String, Object>> rels = new ArrayList<>();
		int i = 0;
		Iterator<Agent> result = result2.iterator();
		while (result.hasNext()) {
			Agent agent = result.next();
			nodes.add(mapStructure("Agent", agent, "rels", "Hardware"));
			int target = i;
			i++;
			for (Relationships role : agent.getRelationship()) {
				Map<String, Object> hardware = mapStructure("Hardware",
						role.getHardware(), "property", role.getId());
				int source = nodes.indexOf(hardware);
				if (source == -1) {
					nodes.add(hardware);
					source = i++;
				}
				rels.add(mapStructure("source", source, "target", target));
			}
		}
		return mapStructure("nodes", nodes, "links", rels);
	}

	private Map<String, Object> mapStructure(String string, Object value,
			String string2, Object value2) {
		Map<String, Object> result = new HashMap<String, Object>(2);
		result.put(string, value);
		result.put(string2, value2);

		return result;
	}

	@Transactional(readOnly = true)
	public Map<String, Object> machine(String errorType) {

		if (errorType.matches("Nodejs")) {
			Collection<ErrorNodejs> result = repositoryerrorsNodejs
					.returnErrorsNodejs();
			return toD3FormatErrorsNodejs(result);
		}
		if (errorType.matches("Websphere")) {
			Collection<ErrorWebsphere> resultados = repositoryerrorsWebsphere
					.getErrorWebsphere();
			return toD3FormatErrorsWebsphere(resultados);
		}
		return null;
	}
}