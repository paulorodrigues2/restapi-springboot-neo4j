package com.dashboard.analytics.Dao;

import java.util.*;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@RelationshipEntity(type = "HAS_ERROR")
public class Role {

	@GraphId
	private Long id;

	private Collection<String> roles = new ArrayList<>();

	@StartNode
	private Nodejs Nodejs;

	@EndNode
	private ErrorNodejs errorsnodejs;

	public Role() {
	}

	public Role(Nodejs Nodejs, ErrorNodejs errorsnodejs) {
		this.errorsnodejs = errorsnodejs;
		this.Nodejs = Nodejs;
	}

	public Long getId() {
		return id;
	}

	public Collection<String> getRoles() {
		return roles;
	}

	public Nodejs getNodejs() {
		return Nodejs;
	}

	public ErrorNodejs getErrorNodejs() {
		return errorsnodejs;
	}

	public void addRoleName(String name) {
		this.roles.add(name);
	}
}
