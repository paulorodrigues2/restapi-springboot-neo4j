package com.dashboard.analytics.Dao;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

@NodeEntity(label = "Hardware")
public class Hardware {
	@GraphId
	Long id;

	public String name;
	@Relationship(type = "CONN", direction = Relationship.INCOMING)
	public String macadress;

	public Hardware() {

	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getMacadress() {
		return macadress;
	}

}
