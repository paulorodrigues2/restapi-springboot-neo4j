package com.dashboard.analytics.Dao;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

@NodeEntity(label = "Machine")
public class Machine {
	@GraphId
	Long id;

	@Relationship(type = "USES")
	private String MacAdress;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMacAdress() {
		return MacAdress;
	}

	public void setMacAdress(String macAdress) {
		MacAdress = macAdress;
	}

}
