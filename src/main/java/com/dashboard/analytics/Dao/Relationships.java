package com.dashboard.analytics.Dao;

import java.util.ArrayList;
import java.util.Collection;

import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

@RelationshipEntity(type = "CONN")
public class Relationships {

	@GraphId
	Long id;

	@StartNode
	Agent agent;
	@EndNode
	Hardware hardware;

	private Collection<String> relationships = new ArrayList<>();

	public Relationships() {

	}

	public Relationships(Agent agent, Hardware hardware) {
		this.agent = agent;
		this.hardware = hardware;

	}

	public Collection<String> getMachineRelationship() {
		return relationships;
	}

	public Long getId() {
		return id;
	}

	public Agent getAgent() {
		return agent;
	}

	public Hardware getHardware() {
		return hardware;
	}

	public void addRelationshipName(String name) {
		this.relationships.add(name);
	}

}
