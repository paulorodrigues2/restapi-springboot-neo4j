package com.dashboard.analytics.Dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.neo4j.ogm.annotation.RelationshipEntity;

@NodeEntity(label = "Agent")
public class Agent {

	@GraphId
	Long id;
	@Relationship(type = "CONN")


	private List<Relationships> relationships = new ArrayList<>();

	public Collection<Relationships> getRelationship() {
		return relationships;
	}

	public void addRole(Relationships rels) {
		this.relationships.add(rels);
	}
	


	private String MacAdress;

	private Long getId() {
		return id;
	}

	private String getMacAdress() {
		return MacAdress;
	}

	private void setMacAdress(String macAdress) {
		MacAdress = macAdress;
	}

}
