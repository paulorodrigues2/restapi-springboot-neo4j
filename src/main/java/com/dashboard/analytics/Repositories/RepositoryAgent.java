package com.dashboard.analytics.Repositories;

import java.util.Collection;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import com.dashboard.analytics.Dao.Agent;

@RepositoryRestResource(collectionResourceRel = "agent", path = "agent")
public interface RepositoryAgent extends GraphRepository<Agent> {

	@Query("MATCH (a:Agent)-[d:CONN]->(h:HARDWARE) RETURN h, d,a ")
	Collection<Agent> returnStructureRest();

}
